#!/bin/bash

[ $1 = '' ]] && BRANCH="master" || BRANCH=$1

SSH_KEY_PATH="eks.pem"
SERVER="ubuntu@13.250.41.98"

ssh -i $SSH_KEY_PATH $SERVER <<-'ENDSSH'
sudo su -
git clone https://gitlab.com/bharathkumar.bablu113/new_project.git
cd new_project
docker-compose up

ENDSSH
